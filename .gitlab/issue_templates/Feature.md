
### Résumé de la fonctionnalité

> Décrivez de manière simple et concise la fonction que vous voulez voir ajoutée.

### Expérience Utilisateur

> Indiquez ce que l'utilisateur aura comme bénéfice avec cette fonction.

### Priority/Importance

> Selon vous, quelle est l'importance de cette fonctionnalité.

/label ~Feature
