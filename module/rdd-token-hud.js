/* -------------------------------------------- */
import { HtmlUtility } from "./html-utility.js";
import { RdDCombatManager } from "./rdd-combat.js";
import { RdDUtility } from "./rdd-utility.js";

/* -------------------------------------------- */
export class RdDTokenHud {

  static init(){
      // Integration du TokenHUD
    Hooks.on('renderTokenHUD', (app, html, data) => { RdDTokenHud.addTokenHudExtensions(app, html, data._id) });
  }

  /* -------------------------------------------- */
  static async removeExtensionHud( app, html, tokenId) {
    let combat = html.find('.control-icon.rdd-combat');
    combat.remove();
    let initiative = html.find('.control-icon.rdd-initiative');
    initiative.remove();
  }

  /* -------------------------------------------- */
  static async addExtensionHud( app, html, tokenId ) {

    let token = canvas.tokens.get(tokenId);
    let actor = token.actor;
    let combatant = game.combat.data.combatants.find(c => c.tokenId == token.data._id);
    app.hasExtension = true;

    let armesList = RdDCombatManager.buildListeActionsCombat(combatant) ;
    const hudData = { combatant: combatant, armes: armesList, 
                      commandes: [{ name: 'Initiative +1', command: 'inc', value: 0.01}, { name: 'Initiative -1',command: 'dec', value: -0.01}] };

    // initiative
    await RdDTokenHud._configureSubMenu(html.find('.control-icon.combat'), 'systems/foundryvtt-reve-de-dragon/templates/hud-actor-init.html', hudData,
      (event) => {
        let initCommand = event.currentTarget.attributes['data-command'].value;
        let combatantId = event.currentTarget.attributes['data-combatant-id'].value;
        if ( !initCommand ) { 
          let armeIndex = event.currentTarget.attributes['data-arme-id'].value;
          let arme = armesList[armeIndex];
          RdDCombatManager.rollInitiativeCompetence(combatantId, arme);          
        } else if (initCommand == 'inc') {
          RdDCombatManager.incDecInit( combatantId, 0.01 );
        } else if ( initCommand == 'dec') {
          RdDCombatManager.incDecInit( combatantId, -0.01 );
        }
      });

    // combat
    await RdDTokenHud._configureSubMenu(html.find('.control-icon.target'), 'systems/foundryvtt-reve-de-dragon/templates/hud-actor-attaque.html', hudData,
      (event) => {
        let armeIndex = event.currentTarget.attributes['data-arme-id'].value;
        let arme = armesList[armeIndex];
        actor.rollArme(arme.data.competence, arme.name);
      });
  }

  /* -------------------------------------------- */
  static async addTokenHudExtensions(app, html, tokenId) {

    html.find('.control-icon.combat').click(event => {
      if ( event.currentTarget.className.includes('active')) {
        RdDTokenHud.removeExtensionHud( app, html, tokenId);
      } else {
        setTimeout( function() { RdDTokenHud.addExtensionHud( app, html, tokenId) } , 200 );
      }
    } );

    let combatIcon  = html.find('.control-icon.combat');
    if ( combatIcon[0].className.includes('active') ) {
      RdDTokenHud.addExtensionHud( app, html, tokenId);
    }
  }

  /* -------------------------------------------- */
  static async _configureSubMenu(insertionPoint, template, hudData, onMenuItem) {
    const hud = $(await renderTemplate(template, hudData));
    const imgHud = hud.find('img.rdd-hud-togglebutton');
    const list = hud.find('div.rdd-hud-list');

    hud.toggleClass('active');
    HtmlUtility._showControlWhen(list, hud.hasClass('active'));

    imgHud.click(event => {
      hud.toggleClass('active');
      HtmlUtility._showControlWhen(list, hud.hasClass('active'));
    });

    list.find('.rdd-hud-menu').click(onMenuItem);

    insertionPoint.after(hud);
  }

}