import { Misc } from "./misc.js";
import { RdDUtility } from "./rdd-utility.js";

/* -------------------------------------------- */
export class RdDItem extends Item {

  /* -------------------------------------------- */
  async postItem() {
    console.log(this);
    const properties = this[`_${this.data.type}ChatData`]();
    const itemData = Misc.data(this);
    let chatData = duplicate(itemData);
    chatData["properties"] = properties

    //Check if the posted item should have availability/pay buttons
    chatData.hasPrice = "cout" in chatData.data;
    chatData.data.cout_deniers = 0;

    let dialogResult = [-1, -1]; // dialogResult[0] = quantité, dialogResult[1] = prix
    if (chatData.hasPrice )
    {
      let sols = chatData.data.cout;
      chatData.data.cout_deniers = Math.floor(sols * 100);
      dialogResult = await new Promise( (resolve, reject) => {new Dialog({
          content : 
          `<p>Modifier la quantité?</p>
          <div class="form-group">
            <label> Quantité</label>
            <input name="quantity" type="text" value="1"/>
          </div>
          <p>Modifier la prix?</p>
          <div class="form-group">
            <label> Prix en Sols</label>
            <input name="price" type="text" value="${chatData.data.cout}"/>
          </div>
          `,
          title : "Quantité & Prix",
          buttons : {
            post : {
              label : "Soumettre",
              callback: (dlg) => {
                resolve( [ dlg.find('[name="quantity"]').val(), dlg.find('[name="price"]').val() ] )
              }
            },
          }
        }).render(true)
      })
    } 
    
    if (dialogResult[0] > 0)
    {
      if (this.isOwned)
      {
        if (itemData.data.quantite == 0)
          dialogResult[0] = -1
        else if (itemData.data.quantite < dialogResult[0])
        {
          dialogResult[0] = itemData.data.quantite;
          ui.notifications.notify(`Impossible de poster plus que ce que vous avez. La quantité à été réduite à ${dialogResult[0]}.`) 
          this.update({"data.quantite" : 0})
        }
        else {
          ui.notifications.notify(`Quantité réduite par ${dialogResult[0]}.`) 
          this.update({"data.quantite" : itemData.data.quantite - dialogResult[0]})
        }
      }
    }
    
    if ( chatData.hasPrice ) {
      if (dialogResult[0] > 0)
        chatData.postQuantity = Number(dialogResult[0]);
      if (dialogResult[1] > 0) {
        chatData.postPrice = dialogResult[1];            
        chatData.data.cout_deniers = Math.floor(dialogResult[1] * 100); // Mise à jour cout en deniers
      }
      chatData.finalPrice = Number(chatData.postPrice) * Number(chatData.postQuantity);
      chatData.data.cout_deniers_total = chatData.data.cout_deniers * Number(chatData.postQuantity);
      chatData.data.quantite = chatData.postQuantity;
      console.log("POST : ", chatData.finalPrice, chatData.data.cout_deniers_total, chatData.postQuantity);
    }
    // Don't post any image for the item (which would leave a large gap) if the default image is used
    if (chatData.img.includes("/blank.png"))
      chatData.img = null;
    
    // JSON object for easy creation
    chatData.jsondata = JSON.stringify(
        {
          compendium : "postedItem",
          payload: itemData,
        });
        
    renderTemplate('systems/foundryvtt-reve-de-dragon/templates/post-item.html', chatData).then(html => {
      let chatOptions = RdDUtility.chatDataSetup(html);
      ChatMessage.create(chatOptions)
    });
  }

  /* -------------------------------------------- */
  _objetChatData() {
    const rddData = Misc.data(this).data;
    let properties = [
      `<b>Encombrement</b>: ${rddData.encombrement}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _armeChatData() {
    const rddData = Misc.data(this).data;
    let properties = [
      `<b>Compétence</b>: ${rddData.competence}`,
      `<b>Dommages</b>: ${rddData.dommages}`,
      `<b>Force minimum</b>: ${rddData.force}`,
      `<b>Resistance</b>: ${rddData.resistance}`,
      `<b>Encombrement</b>: ${rddData.encombrement}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _conteneurChatData() {
    const rddData = Misc.data(this).data;
    let properties = [
      `<b>Capacité</b>: ${rddData.capacite} Enc.`,
      `<b>Encombrement</b>: ${rddData.encombrement}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _munitionChatData() {
    const rddData = Misc.data(this).data;
    let properties = [
      `<b>Encombrement</b>: ${rddData.encombrement}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _armureChatData() {
    const rddData = Misc.data(this).data;
    let properties = [
      `<b>Protection</b>: ${rddData.protection}`,
      `<b>Détérioration</b>: ${rddData.deterioration}`,
      `<b>Malus armure</b>: ${rddData.malus}`,
      `<b>Encombrement</b>: ${rddData.encombrement}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _competenceChatData() {
    const rddData = Misc.data(this).data;
    let properties = [
      `<b>Catégorie</b>: ${rddData.categorie}`,
      `<b>Niveau</b>: ${rddData.niveau}`,
      `<b>Caractéristique par défaut</b>: ${rddData.carac_defaut}`,
      `<b>XP</b>: ${rddData.xp}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _competencecreatureChatData() {
    const rddData = Misc.data(this).data;
    let properties = [
      `<b>Catégorie</b>: ${rddData.categorie}`,
      `<b>Niveau</b>: ${rddData.niveau}`,
      `<b>Caractéristique</b>: ${rddData.carac_value}`,
      `<b>XP</b>: ${rddData.xp}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _sortChatData() {
    const rddData = Misc.data(this).data;
    let properties = [
      `<b>Draconic</b>: ${rddData.draconic}`,
      `<b>Difficulté</b>: ${rddData.difficulte}`,
      `<b>Case TMR</b>: ${rddData.caseTMR}`,
      `<b>Points de Rêve</b>: ${rddData.ptreve}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _herbeChatData() {
    const rddData = Misc.data(this).data;
    let properties = [
      `<b>Milieu</b>: ${rddData.milieu}`,
      `<b>Rareté</b>: ${rddData.rarete}`,
      `<b>Catégorie</b>: ${rddData.categorie}`,
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _ingredientChatData() {
    const rddData = Misc.data(this).data;
    let properties = [
      `<b>Milieu</b>: ${rddData.milieu}`,
      `<b>Rareté</b>: ${rddData.rarete}`,
      `<b>Catégorie</b>: ${rddData.categorie}`,
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _tacheChatData() {
    const rddData = Misc.data(this).data;
    let properties = [
      `<b>Caractéristique</b>: ${rddData.carac}`,
      `<b>Compétence</b>: ${rddData.competence}`,
      `<b>Périodicité</b>: ${rddData.periodicite}`,
      `<b>Fatigue</b>: ${rddData.fatigue}`,
      `<b>Difficulté</b>: ${rddData.difficulte}`,
      `<b>Points de Tâche</b>: ${rddData.points_de_tache}`,
      `<b>Points de Tâche atteints</b>: ${rddData.points_de_tache_courant}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _livreChatData() {
    const rddData = Misc.data(this).data;
    let properties = [
      `<b>Compétence</b>: ${rddData.competence}`,
      `<b>Auteur</b>: ${rddData.auteur}`,
      `<b>Difficulté</b>: ${rddData.difficulte}`,
      `<b>Points de Tâche</b>: ${rddData.points_de_tache}`,
      `<b>Encombrement</b>: ${rddData.encombrement}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _potionChatData() {
    const rddData = Misc.data(this).data;
    let properties = [
      `<b>Rareté</b>: ${rddData.rarete}`,
      `<b>Catégorie</b>: ${rddData.categorie}`,
      `<b>Encombrement</b>: ${rddData.encombrement}`,
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _queueChatData() {
    const rddData = Misc.data(this).data;
    let properties = [
      `<b>Refoulement</b>: ${rddData.refoulement}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _ombreChatData() {
    const rddData = Misc.data(this).data;
    let properties = [
      `<b>Refoulement</b>: ${rddData.refoulement}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _souffleChatData() {
    const rddData = Misc.data(this).data;
    let properties = [];
    return properties;
  }
  /* -------------------------------------------- */
  _teteChatData() {
    const rddData = Misc.data(this).data;
    let properties = [];
    return properties;
  }
  /* -------------------------------------------- */
  _tarotChatData() {
    const rddData = Misc.data(this).data;
    let properties = [
      `<b>Concept</b>: ${rddData.concept}`,
      `<b>Aspect</b>: ${rddData.aspect}`,
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _nombreastralChatData() {
    const rddData = Misc.data(this).data;
    let properties = [
      `<b>Valeur</b>: ${rddData.value}`,
      `<b>Jour</b>: ${rddData.jourlabel}`,
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _monnaieChatData() {
    const rddData = Misc.data(this).data;
    let properties = [
      `<b>Valeur en Deniers</b>: ${rddData.valeur_deniers}`,
      `<b>Encombrement</b>: ${rddData.encombrement}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _meditationChatData() {
    const rddData = Misc.data(this).data;
    let properties = [
      `<b>Thème</b>: ${rddData.theme}`,
      `<b>Compétence</b>: ${rddData.competence}`,
      `<b>Support</b>: ${rddData.support}`,
      `<b>Heure</b>: ${rddData.heure}`,
      `<b>Purification</b>: ${rddData.purification}`,
      `<b>Vêture</b>: ${rddData.veture}`,
      `<b>Comportement</b>: ${rddData.comportement}`,
      `<b>Case TMR</b>: ${rddData.tmr}`
    ]
    return properties;
  }
  /* -------------------------------------------- */
  _casetmrChatData() {
    const rddData = Misc.data(this).data;
    let properties = [
      `<b>Coordonnée</b>: ${rddData.coord}`,
      `<b>Spécificité</b>: ${rddData.specific}`
    ]
    return properties;
  }

}
