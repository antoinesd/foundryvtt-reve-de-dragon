/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */

import { RdDUtility } from "./rdd-utility.js";
import { HtmlUtility } from "./html-utility.js";
import { RdDItemArme } from "./item-arme.js";
import { RdDItemCompetence } from "./item-competence.js";
import { RdDBonus } from "./rdd-bonus.js";
import { Misc } from "./misc.js";
import { RdDCombatManager } from "./rdd-combat.js";
import { RdDCarac } from "./rdd-carac.js";

/* -------------------------------------------- */
export class RdDActorSheet extends ActorSheet {

  /** @override */
  static get defaultOptions() {
    RdDUtility.initAfficheContenu();
    return mergeObject(super.defaultOptions, {
      classes: ["rdd", "sheet", "actor"],
      template: "systems/foundryvtt-reve-de-dragon/templates/actor-sheet.html",
      width: 640,
      //height: 720,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "carac" }],
      dragDrop: [{ dragSelector: ".item-list .item", dropSelector: null }],
      editCaracComp: false,
      showCompNiveauBase: false,
      montrerArchetype: false
    });
  }

  /* -------------------------------------------- */
  async getData() {
    let formData = super.getData();
    // -------------- version 0.7.9
    // let formData = {
    //   cssClass: this.entity.owner ? "editable" : "locked",
    //   editable: this.isEditable,
    //   entity: duplicate(this.entity.data),
    //   limited: this.entity.limited,
    //   options: this.options,
    //   owner: this.entity.owner,
    //   title: this.title
    // }
    // // Entity data
    // formData.actor = formData.entity;
    // formData.data = formData.entity.data;

    // // Owned items
    // formData.items = formData.actor.items;
    // formData.items.sort((a, b) => (a.sort || 0) - (b.sort || 0));

    formData.itemsByType = Misc.classify(formData.items);
    RdDUtility.filterItemsPerTypeForSheet(formData);

    formData.options.isGM = game.user.isGM;

    // la taille est la taille: on ne peut pas l'utiliser pour un jet
    formData.data.carac.taille.isTaille = true;

    if (this.actor.data.type == 'creature') return formData; // Shortcut

    formData.competenceByCategory = Misc.classify(formData.data.competences, it => it.data.categorie);
    
    formData.calc = {
      comptageArchetype: RdDItemCompetence.computeResumeArchetype(formData.data.competences),
      competenceXPTotal: RdDItemCompetence.computeTotalXP(formData.data.competences),
      caracTotal: RdDCarac.computeTotal(formData.data.carac, formData.data.beaute),
      // Mise à jour de l'encombrement total et du prix de l'équipement
      encTotal: await this.actor.computeEncombrementTotalEtMalusArmure(),
      prixTotalEquipement: await this.actor.computePrixTotalEquipement(),
      surprise: RdDBonus.find(this.actor.getSurprise(false)).descr,
      fatigue: {
        malus: RdDUtility.calculMalusFatigue(formData.data.sante.fatigue.value, formData.data.sante.endurance.max),
        html: "<table class='table-fatigue'>" + RdDUtility.makeHTMLfatigueMatrix(formData.data.sante.fatigue.value, formData.data.sante.endurance.max).html() + "</table>"
      },
      resumeBlessures: this.actor.computeResumeBlessure(formData.data.blessures),
    };
    formData.calc.surEncombrementMessage = (formData.data.compteurs.surenc.value < 0) ? "Sur-Encombrement!" : "";

    formData.data.competences.forEach(item => {
      item.visible = !this.options.showCompNiveauBase || !RdDItemCompetence.isNiveauBase(item);
      RdDItemCompetence.levelUp(item);
    });

    Object.values(formData.data.carac).forEach(c => {
      RdDCarac.levelUp(c);
    });


    // toujours avoir une liste d'armes (pour mettre esquive et corps à corps)
    formData.data.combat = duplicate(formData.itemsByType.arme ?? []);
    RdDItemArme.computeNiveauArmes(formData.data.combat, formData.data.competences);
    RdDItemArme.ajoutCorpsACorps(formData.data.combat, formData.data.competences, formData.data.carac );
    formData.esquive = RdDItemCompetence.getEsquive(formData.data.competences);
    formData.data.combat = RdDCombatManager.finalizeArmeList(formData.data.combat, formData.itemsByType.competence, formData.data.carac);


    this.armesList = formData.data.combat;

    // Mise à jour de l'encombrement total et du prix de l'équipement

    // Common data
    formData.data.competenceByCategory = formData.competenceByCategory;
    formData.data.isGM = game.user.isGM;
    formData.ajustementsConditions = CONFIG.RDD.ajustementsConditions;
    formData.difficultesLibres = CONFIG.RDD.difficultesLibres;

    // low is normal, this the base used to compute the grid.
    formData.data.fatigue = {
      malus: RdDUtility.calculMalusFatigue(formData.data.sante.fatigue.value, formData.data.sante.endurance.max),
      html: "<table class='table-fatigue'>" + RdDUtility.makeHTMLfatigueMatrix(formData.data.sante.fatigue.value, formData.data.sante.endurance.max).html() + "</table>"
    }

    formData.hautreve = {
      sortsReserve: formData.data.reve.reserve.list,
      rencontres: duplicate(formData.data.reve.rencontre.list),
      casesTmr: formData.itemsByType.casetmr
    }

    RdDUtility.buildArbreDeConteneur(this, formData);
    formData.subacteurs = {
      vehicules: this.actor.listeVehicules(),
      montures: this.actor.listeMontures(),
      suivants: this.actor.listeSuivants()
    }
    if (this.actor.getBestDraconic().data.niveau > -11 && !this.actor.isHautRevant()) {
      ui.notifications.error(`${this.actor.name} a des compétences draconiques, mais pas le don de Haut-Rêve!
        <br>Ajoutez-lui la tête "Don de Haut-Rêve" pour lui permettre d'utiliser ses compétences et d'accéder aux terres médianes du rêve`);
    }
    return formData;
  }

  isCompetenceAffichable(competence) {
    return !this.options.showCompNiveauBase || !RdDItemCompetence.isNiveauBase(competence);
  }

  /* -------------------------------------------- */
  async _onDrop(event) {
    let toSuper = await RdDUtility.processItemDropEvent(this, event);
    if (toSuper) {
      super._onDrop(event);
    }
  }

  /* -------------------------------------------- */
  async createEmptyTache() {
    await this.actor.createOwnedItem({ name: 'Nouvelle tache', type: 'tache' }, { renderSheet: true });
  }

  /* -------------------------------------------- */
  async creerObjet() {
    let itemType = $(".item-type").val();
    await this.actor.createOwnedItem({ name: 'Nouveau ' + itemType, type: itemType }, { renderSheet: true });
  }

  /* -------------------------------------------- */
  async selectObjetType() {
    let typeObjets = ["objet", "arme", "armure", "conteneur", "herbe", "ingredient", "livre", "potion", "munition", "monnaie"];
    let options = `<span class="competence-label">Selectionnez le type d'équipement</span><select class="item-type">`;
    for (let typeName of typeObjets) {
      options += `<option value="${typeName}">${typeName}</option>`
    }
    options += '</select>';
    let d = new Dialog({
      title: "Créer un équipement",
      content: options,
      buttons: {
        one: {
          icon: '<i class="fas fa-check"></i>',
          label: "Créer l'objet",
          callback: () => this.creerObjet()
        }
      }
    });
    d.render(true);
  }
  
  /* -------------------------------------------- */
  async selectTypeOeuvre() {
    let typeOeuvres = ["oeuvre", "recettecuisine", "musique", "chant", "danse", "jeu" ];
    let options = `<span class="competence-label">Selectionnez le type d'oeuvre</span><select class="item-type">`;
    for (let typeName of typeOeuvres) {
      options += `<option value="${typeName}">${typeName}</option>`
    }
    options += '</select>';
    let d = new Dialog({
      title: "Créer une oeuvre",
      content: options,
      buttons: {
        one: {
          icon: '<i class="fas fa-check"></i>',
          label: "Créer l'oeuvre",
          callback: () => this.creerObjet()
        }
      }
    });
    d.render(true);
  }
  /* -------------------------------------------- */
  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    HtmlUtility._showControlWhen($(".gm-only"), game.user.isGM);

    html.find('#show-hide-competences').click((event) => {
      this.options.showCompNiveauBase = !this.options.showCompNiveauBase;
      this.render(true);
    });

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.getOwnedItem(li.data("item-id"));
      item.sheet.render(true);
    });
    // Update Inventory Item
    html.find('.rencontre-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const rencontreKey = li.data("item-id");
      this.actor.deleteTMRRencontre(rencontreKey);
    });

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      RdDUtility.confirmerSuppression(this, li);
    });
    html.find('.subacteur-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      RdDUtility.confirmerSuppressionSubacteur(this, li);
    });

    html.find('#encaisser-direct').click(ev => {
      this.actor.encaisser();
    });

    html.find('.remise-a-neuf').click(ev => {
      if (game.user.isGM) {
        this.actor.remiseANeuf();
        ev.preventDefault();
      }
    });
    html.find('.creer-tache').click(ev => {
      this.createEmptyTache();
    });
    html.find('.creer-un-objet').click(ev => {
      this.selectObjetType();
    });
    html.find('.creer-une-oeuvre').click(ev => {
      this.selectTypeOeuvre();
    });
    html.find('#nettoyer-conteneurs').click(ev => {
      this.actor.nettoyerConteneurs();
    });

    // Blessure control
    html.find('.blessure-control').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      let btype = li.data("blessure-type");
      let index = li.data('blessure-index');
      let active = $(ev.currentTarget).data('blessure-active');
      //console.log(btype, index, active);
      this.actor.manageBlessureFromSheet(btype, index, active).then(this.render(true));
    });

    // Blessure data
    html.find('.blessures-soins').change(ev => {
      const li = $(ev.currentTarget).parents(".item");
      let btype = li.data('blessure-type');
      let index = li.data('blessure-index');
      let psoins = li.find('input[name=premiers_soins]').val();
      let pcomplets = li.find('input[name=soins_complets]').val();
      let jours = li.find('input[name=jours]').val();
      let loc = li.find('input[name=localisation]').val();
      //console.log(btype, index, psoins, pcomplets, jours, loc);
      this.actor.setDataBlessureFromSheet(btype, index, psoins, pcomplets, jours, loc).then(this.render(true));
    });

    // Equip Inventory Item
    html.find('.item-equip').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      this.actor.equiperObjet(li.data("item-id"));
      this.render(true);
    });

    // Roll Carac
    html.find('.carac-label a').click((event) => {
      let caracName = event.currentTarget.attributes.name.value;
      this.actor.rollCarac(caracName.toLowerCase());
    });

    html.find('.chance-actuelle').click((event) => {
      this.actor.rollCarac('chance-actuelle');
    });

    html.find('.chance-appel').click((event) => {
      this.actor.rollAppelChance();
    });

    html.find('#jet-astrologie').click((event) => {
      this.actor.astrologieNombresAstraux();
    });

    // Roll Skill
    html.find('.competence-label a').click((event) => {
      let compName = event.currentTarget.text;
      this.actor.rollCompetence(compName);
    });
    html.find('.tache-label a').click((event) => {
      const li = $(event.currentTarget).parents(".item");
      let tacheId = li.data('item-id');
      this.actor.rollTache(tacheId);
    });
    html.find('.meditation-label a').click((event) => {
      const li = $(event.currentTarget).parents(".item");
      let meditationId = li.data('item-id');
      this.actor.rollMeditation(meditationId);
    });
    html.find('.chant-label a').click((event) => {
      const li = $(event.currentTarget).parents(".item");
      let chantId = li.data('item-id');
      this.actor.rollChant(chantId);
    });
    html.find('.danse-label a').click((event) => {
      const li = $(event.currentTarget).parents(".item");
      let danseId = li.data('item-id');
      this.actor.rollDanse(danseId);
    });
    html.find('.musique-label a').click((event) => {
      const li = $(event.currentTarget).parents(".item");
      let musiqueId = li.data('item-id');
      this.actor.rollMusique(musiqueId);
    });
    html.find('.oeuvre-label a').click((event) => {
      const li = $(event.currentTarget).parents(".item");
      let oeuvreId = li.data('item-id');
      this.actor.rollOeuvre(oeuvreId);
    });
    html.find('.jeu-label a').click((event) => {
      const li = $(event.currentTarget).parents(".item");
      let jeuId = li.data('item-id');
      this.actor.rollJeu(jeuId);
    });
    html.find('.recettecuisine-label a').click((event) => {
      const li = $(event.currentTarget).parents(".item");
      let recetteId = li.data('item-id');
      this.actor.rollRecetteCuisine(recetteId);
    });
    html.find('.subacteur-label a').click((event) => {
      const li = $(event.currentTarget).parents(".item");
      let actorId = li.data('actor-id');
      let actor = game.actors.get(actorId);
      if (actor) {
        actor.sheet.render(true);
      }
    });

    // Points de reve actuel
    html.find('.ptreve-actuel a').click((event) => {
      this.actor.rollCarac('reve-actuel');
    });

    // Roll Weapon1
    html.find('.arme-label a').click((event) => {
      let armeName = event.currentTarget.text;
      let competenceName = event.currentTarget.attributes['data-competence-name'].value;
      this.actor.rollArme(competenceName, armeName);
    });
    // Initiative pour l'arme
    html.find('.arme-initiative a').click((event) => {
      let combatant = game.combat.data.combatants.find(c => c.actor.data._id == this.actor.data._id);
      if (combatant) {
        let armeName = event.currentTarget.attributes['data-arme-name'].value;
        let arme = this.armesList.find(a => a.name == armeName);
        RdDCombatManager.rollInitiativeCompetence(combatant._id, arme);
      } else {
        ui.notifications.info("Impossible de lancer l'initiative sans être dans un combat.");
      }
    });
    // Display TMR, visuualisation
    html.find('#visu-tmr').click((event) => {
      this.actor.displayTMR("visu");
    });

    // Display TMR, normal
    html.find('#monte-tmr').click((event) => {
      this.actor.displayTMR("normal");
    });

    // Display TMR, fast 
    html.find('#monte-tmr-rapide').click((event) => {
      this.actor.displayTMR("rapide");
    });

    html.find('#dormir-une-heure').click((event) => {
      this.actor.dormir(1);
    });
    html.find('#dormir-chateau-dormant').click((event) => {
      this.actor.dormirChateauDormant();
    });
    html.find('#enlever-tous-effets').click((event) => {
      this.actor.enleverTousLesEffets();
    });
    // Display info about queue
    html.find('.queuesouffle-label a').click((event) => {
      let myID = event.currentTarget.attributes['data-item-id'].value;
      const item = this.actor.getOwnedItem(myID);
      item.sheet.render(true);
    });
    // Info sort
    html.find('.sort-label a').click((event) => {
      let myID = event.currentTarget.attributes['data-id'].value;
      const item = this.actor.getOwnedItem(myID);
      item.sheet.render(true);
    });
    // Info sort
    html.find('.case-label a').click((event) => {
      let myID = event.currentTarget.attributes['data-id'].value;
      const item = this.actor.getOwnedItem(myID);
      item.sheet.render(true);
    });

    // Display info about queue
    html.find('.conteneur-name a').click((event) => {
      let myID = event.currentTarget.attributes['data-item-id'].value;
      RdDUtility.toggleAfficheContenu(myID);
      this.render(true);
    });

    if (this.options.editCaracComp) {
      // On carac change
      html.find('.carac-value').change((event) => {
        let caracName = event.currentTarget.name.replace(".value", "").replace("data.carac.", "");
        //console.log("Value changed :", event, caracName);
        this.actor.updateCarac(caracName, parseInt(event.target.value));
      });
      html.find('.carac-xp').change((event) => {
        let caracName = event.currentTarget.name.replace(".xp", "").replace("data.carac.", "");
        //console.log("Value changed :", event, caracName);
        this.actor.updateCaracXP(caracName, parseInt(event.target.value));
      });
      // On competence change
      html.find('.competence-value').change((event) => {
        let compName = event.currentTarget.attributes.compname.value;
        //console.log("Competence changed :", compName);
        this.actor.updateCompetence(compName, parseInt(event.target.value));
      });
      // On competence xp change
      html.find('.competence-xp').change((event) => {
        let compName = event.currentTarget.attributes.compname.value;
        this.actor.updateCompetenceXP(compName, parseInt(event.target.value));
      });
      // On competence xp change
      html.find('.competence-xp-sort').change((event) => {
        let compName = event.currentTarget.attributes.compname.value;
        this.actor.updateCompetenceXPSort(compName, parseInt(event.target.value));
      });
      // On competence archetype change
      html.find('.competence-archetype').change((event) => {
        let compName = event.currentTarget.attributes.compname.value;
        this.actor.updateCompetenceArchetype(compName, parseInt(event.target.value));
      });
    }

    // Gestion du bouton lock/unlock
    html.find('.lock-unlock-sheet').click((event) => {
      this.options.editCaracComp = !this.options.editCaracComp;
      this.render(true);
    });
    html.find('#show-hide-archetype').click((event) => {
      this.options.montrerArchetype = !this.options.montrerArchetype;
      this.render(true);
    });

    // On pts de reve change
    html.find('.pointsreve-value').change((event) => {
      let reveValue = event.currentTarget.value;
      this.actor.update({ "data.reve.reve.value": reveValue });
    });

    // On seuil de reve change
    html.find('.seuil-reve-value').change((event) => {
      console.log("seuil-reve-value", event.currentTarget)
      this.actor.setPointsDeSeuil(event.currentTarget.value);
    });

    html.find('#attribut-protection-edit').change((event) => {
      this.actor.updateAttributeValue(event.currentTarget.attributes.name.value, parseInt(event.target.value));
    });

    // On stress change
    html.find('.compteur-edit').change((event) => {
      let fieldName = event.currentTarget.attributes.name.value;
      this.actor.updateCompteurValue(fieldName, parseInt(event.target.value));
    });

    html.find('#ethylisme').change((event) => {
      this.actor.setEthylisme(parseInt(event.target.value));
    });
    html.find('.stress-test').click((event) => {
      this.actor.transformerStress();
      this.render(true);
    });
    html.find('.moral-malheureux').click((event) => {
      this.actor.jetDeMoral('malheureuse');
      this.render(true);
    });
    html.find('.moral-neutre').click((event) => {
      this.actor.jetDeMoral('neutre');
      this.render(true);
    });
    html.find('.moral-heureux').click((event) => {
      this.actor.jetDeMoral('heureuse');
      this.render(true);
    });
    html.find('#ethylisme-test').click((event) => {
      this.actor.ethylismeTest();
      this.render(true);
    });

    html.find('#jet-vie').click((event) => {
      this.actor.jetVie();
      this.render(true);
    });
    html.find('#jet-endurance').click((event) => {
      this.actor.jetEndurance();
      this.render(true);
    });

    html.find('.monnaie-plus').click((event) => {
      const li = $(event.currentTarget).parents(".item");
      this.actor.monnaieIncDec(li.data("item-id"), 1);
      this.render(true);
    });
    html.find('.monnaie-moins').click((event) => {
      const li = $(event.currentTarget).parents(".item");
      this.actor.monnaieIncDec(li.data("item-id"), -1);
      this.render(true);
    });

    html.find('#vie-plus').click((event) => {
      this.actor.santeIncDec("vie", 1);
      this.render(true);
    });
    html.find('#vie-moins').click((event) => {
      this.actor.santeIncDec("vie", -1);
      this.render(true);
    });
    html.find('#endurance-plus').click((event) => {
      this.actor.santeIncDec("endurance", 1);
      this.render(true);
    });
    html.find('#endurance-moins').click((event) => {
      this.actor.santeIncDec("endurance", -1);
      this.render(true);
    });
    html.find('.data-sante-sonne').click((event) => {
      this.actor.setSonne(event.currentTarget.checked);
      this.render(true);
    });
    html.find('#ptreve-actuel-plus').click((event) => {
      this.actor.reveActuelIncDec(1);
      this.render(true);
    });
    html.find('#ptreve-actuel-moins').click((event) => {
      this.actor.reveActuelIncDec(-1);
      this.render(true);
    });
    html.find('#fatigue-plus').click((event) => {
      this.actor.santeIncDec("fatigue", 1);
      this.render(true);
    });
    html.find('#fatigue-moins').click((event) => {
      this.actor.santeIncDec("fatigue", -1);
      this.render(true);
    });
  }


  /* -------------------------------------------- */
  /** @override */
  setPosition(options = {}) {
    const position = super.setPosition(options);
    const sheetBody = this.element.find(".sheet-body");
    const bodyHeight = position.height - 192;
    sheetBody.css("height", bodyHeight);
    return position;
  }


  /* -------------------------------------------- */
  /** @override */
  _updateObject(event, formData) {
    // Update the Actor
    return this.object.update(formData);
  }
}
