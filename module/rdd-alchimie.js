/* -------------------------------------------- */
import { Misc } from "./misc.js";

/* -------------------------------------------- */
export class RdDAlchimie {

  /* -------------------------------------------- */
  static processManipulation( recette, actorId = undefined ) {
    //console.log("CALLED", recette, recette.isOwned, actorId );
    let manip = duplicate(recette.data.manipulation);
    let reg1 = new RegExp(/@(\w*){([\w\-]+)}/ig);
    let matchArray = manip.match( reg1 );
    if ( matchArray ) {
      for( let matchStr of matchArray) {
        let reg2 = new RegExp(/@(\w*){([\w\-]+)}/i);
        let result = matchStr.match(reg2);
        //console.log("RESULT ", result);
        if ( result[1] && result[2]) {
          let commande = Misc.upperFirst( result[1] );
          let replacement = this[`_alchimie${commande}`](recette, result[2], actorId);
          manip = manip.replace( result[0], replacement);
        }
      }
    }
    recette.data.manipulation_update = manip;
  }

  /* -------------------------------------------- */
  static _alchimieCouleur( recette, couleurs, actorId ) {
    let replacement
    if ( actorId ) {
      replacement = `<span class="alchimie-tache"><a data-recette-id="${recette._id}" data-actor-id="${actorId}" data-alchimie-tache="couleur" data-alchimie-data="${couleurs}">couleur ${couleurs}</a></span>`;
    } else {
      replacement = `<span class="alchimie-tache">couleur ${couleurs} </span>`;
    }
    return replacement;
  }

  /* -------------------------------------------- */
  static _alchimieConsistance( recette, consistances, actorId ) {
    let replacement
    if ( actorId ) {
      replacement = `<span class="alchimie-tache"><a data-recette-id="${recette._id}" data-actor-id="${actorId}" data-alchimie-tache="consistance" data-alchimie-data="${consistances}">consistance ${consistances}</a></span>`;
    } else {
      replacement = `<span class="alchimie-tache">consistance ${consistances} </span>`;
    }
    return replacement;

  }

  /* -------------------------------------------- */
  static getDifficulte(aspects) {
    let elements = aspects.split('-');
    let composantes = elements.length;
    let distincts = Object.keys(Misc.classifyFirst(elements, it => it)).length;
    if (distincts == 1) {
      composantes--;
    }
    return Math.min(0, -composantes);
  }
}
