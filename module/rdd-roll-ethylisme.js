import { RdDResolutionTable } from "./rdd-resolution-table.js";
import { Misc } from "./misc.js";

/**
 * Extend the base Dialog entity by defining a custom window to perform roll.
 * @extends {Dialog}
 */
export class RdDRollDialogEthylisme extends Dialog {

  /* -------------------------------------------- */
  constructor(html, rollData, actor) {

    let myButtons = {
        rollButton: { label: "Test d'éthylisme", callback: html => this.actor.performEthylisme(this.rollData) }
      };

    // Common conf
    let dialogConf = { content: html, title: "Test d'éthylisme", buttons: myButtons, default: "rollButton" };
    let dialogOptions = { classes: ["rdddialog"], width: 400, height: 220, 'z-index': 99999 }  
    super(dialogConf, dialogOptions)
    
    //console.log("ETH", rollData);
    this.rollData = rollData;
    this.actor = actor;
  }

  /* -------------------------------------------- */
  activateListeners(html) {
    super.activateListeners(html);
    
    this.bringToTop(); // Ensure top level
    // Get the rollData stuff
    var rollData = this.rollData;

    function updateRollResult(rollData) {
      
      rollData.finalLevel = Number(rollData.etat) + Number(rollData.forceAlcool) + rollData.diffNbDoses;
      
      // Mise à jour valeurs
      $("#roll-param").text(rollData.vieValue + " / " + Misc.toSignedString(rollData.finalLevel));
      $(".table-resolution").remove();
      $("#resolutionTable").append(RdDResolutionTable.buildHTMLTableExtract(rollData.vieValue, rollData.finalLevel));
    }

    // Setup everything onload
    $(function () {
      $("#forceAlcool").val(Misc.toInt(rollData.forceAlcool));
      updateRollResult(rollData);
    });
    
    // Update !
    html.find('#forceAlcool').change((event) => {
      rollData.forceAlcool = Misc.toInt(event.currentTarget.value); // Update the selected bonus/malus
      updateRollResult(rollData);
    });
  }

}
