import { Grammar } from "../grammar.js";
import { tmrColors, tmrConstants, tmrTokenZIndex, TMRUtility } from "../tmr-utility.js";
import { Draconique } from "./draconique.js";

export class PontImpraticable extends Draconique {

  constructor() {
    super();
  }

  type() { return 'souffle' }
  match(item) { return Draconique.isSouffleDragon(item) && Grammar.toLowerCaseNoAccent(item.name).includes('impraticabilite des ponts'); }

  async onActorCreateOwned(actor, souffle) {
    const ponts = TMRUtility.getListTMR('pont');
    for (let tmr of ponts) {
      await this.createCaseTmr(actor, 'Pont impraticable: ' + tmr.label, tmr, souffle._id);
    }
  }

  code() { return 'pont-impraticable' }
  tooltip(linkData) { return `${this.tmrLabel(linkData)} impraticable` }
  img() { return 'systems/foundryvtt-reve-de-dragon/icons/tmr/wave.svg' }

  createSprite(pixiTMR) {
    return pixiTMR.sprite(this.code(),
      {
        zIndex: tmrTokenZIndex.casehumide,
        color: tmrColors.casehumide,
        alpha: 0.5,
        taille: tmrConstants.twoThird,
        decallage: tmrConstants.bottom
      });
  }

  async _creerCaseTmr(actor) {
  }

}
